+++
author = "Wildan Fauzy"
categories = []
date = 2020-10-30T05:00:00Z
description = "malam yang panjang, daerah kuburan pun ramai, banyak orang berziarah atau berjualan berbagai macam aneka barang dan makanan."
image = ""
title = "jumat"
type = "post"

+++
bisa dibilang malam jumat sama kedudukannya dengan malam minggu, sebab tidak semua libur pada hari minggu, rata-rata hari jumat adalah hari libur kalau disini.

malam yang panjang, daerah kuburan pun ramai, banyak orang berziarah atau berjualan berbagai macam aneka barang dan makanan.

begitu pula dengan berkumpul orang yang kesepian, sudah jam setengah dua belas malam, terasa hampar jika hanya ditemani seekor cicak yang buntung.

keluar rumah iseng-iseng apakah warkop masih buka, ternyata masih ramai, beberapa orang tidak mengenakan baju karena kegerahan tapi selalu mengeluh digigit nyamuk, akhirnya mandi dengan autan.

memesan secangkir kopi dan satu gelas mineral, tapi sebelumnya menunggu mie indomie empal gentong plus telur setengah matang, sehabis makan tunggu beberapa menit baru ngopi.

setengah jam kemudian perut mules, tidak ada salahnya tidur, ketika melihat jam sudah pukul enam pagi, lalu kaget terbangun cuaca gelap hujan besar dikira sudah sore, pas lihat jam masih pukul sembilan sialan.