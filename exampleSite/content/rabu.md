+++
author = "Wildan Fauzy"
categories = []
date = 2020-10-28T05:00:00Z
description = "entah mengapa hari ini sejak pergantian hari, rasanya seperti dihantam nuklir buatan cina, gelap dan sesak, tersisa teruntuhan, mungkin karena tidak ada lagi rabu dan sabtu."
image = ""
title = "rabu"
type = "post"

+++
entah mengapa hari ini sejak pergantian hari, rasanya seperti dihantam nuklir buatan cina, gelap dan sesak, tersisa teruntuhan, mungkin karena tidak ada lagi rabu dan sabtu.

mata perih oleh serpihan kaca yang pecah, kelenjar kantung mata sudah mengering, dihisap oleh awan, agar besok bisa hujan tapi itu nanti.

hal yang aneh serangan nuklir menancap di tengah-tengah dada, tidak ada yang bisa mengangkat, seperti tertanam di dasar bumi.